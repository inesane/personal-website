
var arr = new Array();
function addData() {
    getData();
    arr.push({
        name: document.getElementById("name").value,
        skill: document.getElementById("skill").value,
        level: document.getElementById("level").value,
        comment: document.getElementById("comment").value
    });
    localStorage.setItem("localData", JSON.stringify(arr));
    showData();
}
function getData() {
    var str = localStorage.getItem("localData");
    if (str != null) {
        arr = JSON.parse(str)
    }
}
function deleteData() {
    localStorage.clear()
}
function showData() {
    getData();
    var tbl = document.getElementById("myTable");
    var x = tbl.rows.length;
    while (--x) {
        tbl.deleteRow(x);
    }
    for (i = 0; i < arr.length; i++) {
        var r = tbl.insertRow();
        var cell1 = r.insertCell();
        var cell2 = r.insertCell();
        var cell3 = r.insertCell();
        var cell4 = r.insertCell();

        cell1.innerHTML = arr[i].name;
        cell2.innerHTML = arr[i].skill;
        cell3.innerHTML = arr[i].level;
        cell4.innerHTML = arr[i].comment;
    }

    /* var r = tbl.insertRow();
     var cell1= r.insertCell();
     var cell2= r.insertCell();
     var cell3= r.insertCell();
     var cell4= r.insertCell();
   
     cell1.innerHTML = document.getElementById("name").value;
     cell2.innerHTML = document.getElementById("skill").value;
     cell3.innerHTML = document.getElementById("level").value;
     cell4.innerHTML = document.getElementById("comment").value;
   */
}

window.onload = showData;
